
import scala.io.Source

val lenPreference = 1 // how much to favor longer instances
val keepCount = 512 // how many prefixes and postfixes to keep

def uniqueOccurences(words:Array[String]):Array[(String, Int)] = words
  .map(word => (word, 1))
  .groupBy(_._1)
  .mapValues(_.length)
  .toArray

def topUniqueOccurences(words:Array[String]):Array[(String, Int)] =
  uniqueOccurences(words)
    .sortBy(x => math.pow(x._1.length, lenPreference) * x._2)
    .reverse
    .take(keepCount)

def firstMatchLength(words:Iterator[String], set:Set[String]):Int = words
  .find(set.contains)
  .map(_.length)
  .getOrElse(0)


val lines:Array[String] = Source.fromInputStream(System.in).getLines()
    .map(_.toLowerCase)
    .toArray
    .distinct

val prefixes = topUniqueOccurences(lines.flatMap(_.inits))
println("Prefixes count: " + prefixes.mkString(", "))

val postfixes = topUniqueOccurences(lines.flatMap(_.tails))
println("Postfixes count: " + postfixes.mkString(", "))

val postfixesSet = postfixes.map(_._1).toSet
val prefixesSet = prefixes.map(_._1).toSet

val roots = lines
  .map(word => word.drop(firstMatchLength(word.inits, prefixesSet)))
  .map(word => word.dropRight(firstMatchLength(word.tails, postfixesSet)))
  .distinct

println("Roots:")
roots.foreach(println)
